#! /usr/bin/env python
# -*- coding: utf-8 -*-
import time

try:
    import Image
except ImportError:
    from PIL import Image

import wsgiserver
import telebot
from common import *

try:
    import Image
except ImportError:
    from PIL import Image


class TB(object):
    def __init__(self):
        from telebot import types
        self.tb = telebot.TeleBot(tb_token2)

    def send_message(self, msg):
        self.tb.send_message(chat_id, msg)

    def send_photo(self, path):
        photo = open('%s' % path, 'rb')
        self.tb.send_photo(chat_id, photo)

    def send_video(self, path):
        video = open('%s' % path, 'rb')
        self.tb.send_video(chat_id, video)


def my_app(environ, start_response):
    status = '200 OK'
    response_headers = [('Content-type', 'text/plain')]
    start_response(status, response_headers)
    tb = TB()

    if environ['PATH_INFO'][1:-1] == "favicon.ic":
        return [b'favicon']

    print(environ)
    query = environ['QUERY_STRING'].split('&')

    status = query[0]
    cam_name = query[1]
    cam_name_ru = ""
    # нормализуем url
    cam_video = query[2].replace('%2f', '/')

    if cam_name == 'komnata':
        cam_name_ru = 'Комната'
    elif cam_name == 'spalnya':
        cam_name_ru = 'Спальня'
    elif cam_name == 'prih':
        cam_name_ru = 'Прихожая\Вход'
    elif cam_name == 'kyhnya':
        cam_name_ru = 'Кухня'

    if status == 'alert_start':
        tb.send_message("Возмущение в силе, [%s]" % cam_name_ru)
    elif status == 'record_finish':
        tb.send_video(cam_video)
    elif status == 'record_start':
        pass
    return [b'WSGIserver is running!']


server = wsgiserver.WSGIServer(my_app, host='', port='')
server.start()
