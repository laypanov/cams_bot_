#! /usr/bin/env python
# -*- coding: utf-8 -*-
import random

import wsgiserver
import telebot
import cv2
import os
from common import *

try:
    import Image
except ImportError:
    from PIL import Image

ret_msg = None

class TB(object):
    def __init__(self):
        from telebot import types
        self.tb = telebot.TeleBot(tb_token)

        markup = types.ReplyKeyboardMarkup(row_width=2)

        itembtn1 = types.KeyboardButton('Показать_[Прихожая]')
        itembtn2 = types.KeyboardButton('Показать_[Комната]')
        itembtn3 = types.KeyboardButton('Показать_[Кухня]')
        itembtn4 = types.KeyboardButton('Показать_[Спальня]')
        itembtn5 = types.KeyboardButton('Показать_[Все]')
        markup.add(itembtn1, itembtn2, itembtn3, itembtn4, itembtn5)

        ret_msg = self.tb.send_message(chat_id, "Выберите", reply_markup=markup)

        @self.tb.message_handler(func=lambda message: True)
        def get_answer(message):
            if message.text == 'Показать_[Все]' \
                    or message.text == 'Показать_[Прихожая]' \
                    or message.text == 'Показать_[Комната]' \
                    or message.text == 'Показать_[Кухня]' \
                    or message.text == 'Показать_[Спальня]':
                photos = Cams.get_frame_cams(self, '%s' % message.text)
                for photo in photos:
                    self.send_photo(photo)

        self.tb.infinity_polling(True)

    def send_message(self, msg):
        self.tb.send_message(chat_id, "text")

    def send_photo(self, path):
        photo = open('%s' % path, 'rb')
        self.tb.send_photo(chat_id, photo)

    def send_video(self, path):
        video = open('%s' % path, 'rb')
        self.tb.send_video(chat_id, video)


class Cams(object):
    def __init__(self):
        self.get_frame_cams()

    def get_frame_cams(self, t):
        cams_ips = []

        if t == 'Показать_[Все]':
            cams_ips = cams_ev
        elif t == 'Показать_[Прихожая]':
            cams_ips = cam_prih
        elif t == 'Показать_[Комната]':
            cams_ips = cam_komnata
        elif t == 'Показать_[Кухня]':
            cams_ips = cam_kyhnya
        elif t == 'Показать_[Спальня]':
            cams_ips = cam_spalnya

        dirname = './jpgs'
        photos = []

        for cam_ip in cams_ips:
            cap = cv2.VideoCapture(
                "rtsp://%s:%s@%s:554/cam/realmonitor?channel=1&subtype=0" % (cam_login, cam_pass, cam_ip)
            )

            while cap.isOpened():
                ret, frame = cap.read()
                if not ret:

                    break
                else:
                    name = "rec_frame_%s.jpg" % random.random()
                    path = os.path.join(dirname, name)
                    cv2.imwrite(path, frame)
                    photos.append(path)
                    break
        cv2.destroyAllWindows()
        return photos
TB()
